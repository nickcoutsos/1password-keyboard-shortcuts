export const createNotificationArea = () => {
  const div = document.createElement('div')
  div.id = 'notification-area'
  document.body.appendChild(div)
}

export const createNotification = (text, className) => {
  const notificationArea = document.querySelector('#notification-area')
  const notification = document.createElement('div')
  notification.classList.add('notification')
  notification.classList.add(className)
  notification.innerHTML = text
  notificationArea.insertBefore(notification, notificationArea.children[0])

  setTimeout(() => notification.classList.add('fading'), 2000)
  setTimeout(() => notificationArea.removeChild(notification), 3000)
}
