import keyboard from 'keyboardjs'
import { createNotificationArea } from './notifications'
import home from './home'
import vault from './vault'

const style = document.createElement('style')
style.type = 'text/css'
style.innerHTML = `
  div#home-card-container .card {
    display: block;
    width: 100%;
    max-width: 100%;
    min-height: 0;
    flex: none;
  }
  div#home-card-container .card .main h2 { margin-top: 15px }
  div#home-card-container .card .main p.description { margin: 0 }

  div#home-card-container .card.selected .header::before {
    border-top-color: #A7B4E0;
  }
  div#home-card-container .card.selected .header::after {
    background-color: #D9D9FD;
  }
  div#home-card-container .card.selected .header .circle {
    border-color: #A7B4E0;
    background-color: #D9D9FD;
  }
  div#home-card-container .card.selected, div#home-card-container .card:hover {
    border-color: #A7B4E0;
  }
  div#home-card-container .card.selected .header, div#home-card-container .card:hover .header {
    border-bottom-color: #A7B4E0;
    background-color: #C0CCFB;
  }

  div#home-card-container .card .accessor-list, div#home-card-container .card h2+p {
    display: none;
  }

  #notification-area {
    position: absolute;
    top: 80px;
    right: 20px;
    text-align: right;
  }

  .notification {
    font-weight: bold;
    padding: 4px;
  }
  .notification.success {
    color: #1a8cff;
  }
  .notification.failure {
    color: red;
  }

  .notification.fading {
    opacity: 0;
    transition: 1s ease-in;
  }

  .selected {
    background-color: transparent;
  }
  #sidebar.active .selected, .active #item-list .selected {
    background-color: rgba(147, 201, 255, 0.62) !important;
  }
  #item-details.active tr.selected td:first-child {
    background-color: rgba(147, 201, 255, 0.62) !important;
  }
`

document.head.appendChild(style)
createNotificationArea()

const views = {
  'account-view': home,
  'vault-view': vault
}

keyboard.setContext()

const observer = new MutationObserver(mutationList => {
  for (let mutation of mutationList) {
    if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
      // hope it doesn't have more than one class
      const prev = mutation.oldValue
      const next = mutation.target.className

      if (views[prev] && views[prev].remove) {
        views[prev].remove()
      }

      if (views[next]) {
        views[next]()
      }
    }
  }
})

observer.observe(document.body, { attributes: true })
