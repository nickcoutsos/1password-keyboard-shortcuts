import keyboard from 'keyboardjs'
import { scrollIntoViewIfNeeded } from './util'

const getSelectedItem = () => {
  return document.querySelector('#home-card-container .card.selected')
}

const openSelectedItem = () => {
  const item = getSelectedItem()
  if (item) {
    item.querySelector('button').click()
  }
}

const selectNextItem = () => {
  const current = getSelectedItem()
  const element = current
    ? current.nextSibling || current.parentNode.firstChild
    : document.querySelector('#home-card-container .card:first-child')

  current && current.classList.remove('selected')
  element.classList.add('selected')
  scrollIntoViewIfNeeded(element, false)
}

const selectPrevItem = () => {
  const current = getSelectedItem()
  const element = current
    ? current.previousSibling || current.parentNode.lastChild
    : document.querySelector('#home-card-container .card:last-child')

  current && current.classList.remove('selected')
  element.classList.add('selected')
  scrollIntoViewIfNeeded(element, true)
}

const preventDefault = fn => e => (e.preventDefault(), fn(e))

keyboard.setContext('home')
keyboard.bind('j', selectNextItem)
keyboard.bind('k', selectPrevItem)
keyboard.bind('o', openSelectedItem)
keyboard.bind('enter', preventDefault(openSelectedItem))

export default function () {
  keyboard.setContext('home')
}
