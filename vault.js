/* global GM */

import keyboard from 'keyboardjs'
import debounce from 'lodash/debounce'
import { scrollIntoViewIfNeeded, onElementFound } from './util'
import { createNotification } from './notifications'

const isTextSelected = () => window.getSelection().toString().length > 0

const getSelectorPrefixForSection = section => {
  switch (section.id) {
    case 'sidebar':
      return 'li'
    case 'item-details':
      return 'tr'
    default:
      return '#item-list li'
  }
}

const getDefaultSection = () => document.querySelector('#vault-content > div')

const getActiveSection = () => document.querySelector('#vault-content .active')

const selectNextSection = () => {
  const current = getActiveSection()
  const target = current
    ? current.nextSibling || current.parentNode.firstChild
    : getDefaultSection()

  current && current.classList.remove('active')
  target && target.classList.add('active')
  ensureSelectedItem(target)
}

const selectPrevSection = () => {
  const current = getActiveSection()
  const target = current
    ? current.previousSibling || current.parentNode.lastChild
    : getDefaultSection()

  current && current.classList.remove('active')
  target && target.classList.add('active')
  ensureSelectedItem(target)
}

const getSectionItems = section => Array.from(section.querySelectorAll(getSelectorPrefixForSection(section)))
const getSelectedItem = section => {
  return section.querySelector('#item-list'))
    ? section.querySelector('li.selected')
    : section.querySelector('.selected')
}

const ensureSelectedItem = section => {
  const item = getSelectedItem(section) || getSectionItems(section)[0]
  item && item.classList.add('selected')
}

const openSelectedItem = debounce(() => {
  const section = getActiveSection() || getDefaultSection()
  const item = getSelectedItem(section) || getSectionItems(section)[0]
  if (item) {
    (item.querySelector('button') || item).click()
  }
}, 250)

const unselectAll = section => {
  Array.from(section.querySelectorAll('.selected'))
    .forEach(element => {
      element.classList.remove('selected')
    })
}

const selectNextItem = () => {
  const section = getActiveSection() || getDefaultSection()
  const sectionItems = getSectionItems(section)
  const current = getSelectedItem(section)
  const currentIndex = sectionItems.indexOf(current)
  const element = sectionItems[currentIndex + 1] || sectionItems[0]

  unselectAll(section)
  element && element.classList.add('selected')

  scrollIntoViewIfNeeded(element, false)
  openSelectedItem()
}

const selectPrevItem = () => {
  const section = getActiveSection() || getDefaultSection()
  const sectionItems = getSectionItems(section)
  const current = getSelectedItem(section)
  const currentIndex = sectionItems.indexOf(current)
  const element = sectionItems[currentIndex - 1] || sectionItems.slice(-1)[0]

  unselectAll(section)
  element && element.classList.add('selected')

  scrollIntoViewIfNeeded(element, true)
  openSelectedItem()
}

const getValueByFieldName = (field) => {
  const query = `//tr[contains(.,'${field}')]`
  const context = document.querySelector('#item-details')
  const results = document.evaluate(query, context, null, XPathResult.ANY_TYPE, null)
  const match = results.iterateNext()

  if (!match) {
    return
  }

  return getFieldValue(match)
}

const getFieldValue = field => {
  const valueNode = field.children[1]

  return valueNode.dataset.copyText || valueNode.innerText
}

const copyValueByFieldName = field => {
  const value = getValueByFieldName(field)
  value && GM.setClipboard(value)
  return true
}

const copyActiveFieldValue = () => {
  const field = document.querySelector('#item-details.active .selected')

  if (!field) {
    return
  }

  GM.setClipboard(getFieldValue(field))
  createNotification(`Copied ${field.children[0].innerText}`, 'success')
}

const ignoreInput = fn => e => ['INPUT', 'SELECT', 'TEXTAREA'].indexOf(e.target.nodeName) === -1 && fn(e)
const skipWhileSelecting = fn => e => !isTextSelected() && fn(e)
const preventDefault = fn => e => (e.preventDefault(), fn(e))
const copyUsername = () => copyValueByFieldName('username') && createNotification('Copied username', 'success')
const copyPassword = () => copyValueByFieldName('password') && createNotification('Copied password', 'success')
const openWebsite = () => {
  const url = (
    getValueByFieldName('website') ||
    getValueByFieldName('url') ||
    getValueByFieldName('URL')
  )

  if (url) {
    GM.openInTab(url, false)
  }
}

const editSelectedItem = (e) => {
  e.preventDefault()
  document.querySelector('button.edit').click()
}

keyboard.setContext('vault')
keyboard.bind('/', ignoreInput(function (e) {
  document.querySelector('#search input').focus()
  e.preventDefault()
}))

keyboard.bind('j', ignoreInput(selectNextItem))
keyboard.bind('k', ignoreInput(selectPrevItem))
keyboard.bind('n', ignoreInput(selectNextSection))
keyboard.bind('p', ignoreInput(selectPrevSection))
keyboard.bind('e', ignoreInput(editSelectedItem))
keyboard.bind('command + u', preventDefault(openWebsite))
keyboard.bind('command + c', skipWhileSelecting(copyPassword))
keyboard.bind('command + b', preventDefault(copyUsername))

window.addEventListener('keydown', e => {
  if (e.target.matches('#search input')) {
    if (e.key === 'Enter') {
      e.target.blur()
    } else if (e.key === 'Escape') {
      e.target.value = ''
      e.target.blur()
      e.target.dispatchEvent(new Event('change', { bubbles: true }))
    }
  } else if (document.querySelector('#item-details.active')) {
    if (e.key === 'Enter') {
      copyActiveFieldValue()
    }
  }
})

export default function () {
  keyboard.setContext('vault')
  const oldActive = document.querySelector('.active')
  oldActive && oldActive.classList.remove('active')

  onElementFound('#vault-content > div', 150, element => {
    element.classList.add('active')
  })
}
