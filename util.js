export function scrollIntoViewIfNeeded (element, alignToTop) {
  const scroll = element.offsetParent.scrollTop
  const height = element.offsetParent.offsetHeight
  const top = element.offsetTop
  const bottom = top + element.scrollHeight

  if (top < scroll || bottom > scroll + height) {
    element.scrollIntoView(alignToTop)
  }
}

export function onElementFound (selector, interval, callback) {
  const interval_ = setInterval(function () {
    const element = document.querySelector(selector)
    if (element) {
      clearInterval(interval_)
      callback(element)
    }
  }, interval)
}
